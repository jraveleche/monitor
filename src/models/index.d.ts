import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";

export enum RolColaborador {
  ADMINISTRADOR = "ADMINISTRADOR",
  ENCARGADO = "ENCARGADO",
  AYUDANTE = "AYUDANTE",
  PROMOTOR = "PROMOTOR"
}

export enum EstadoInspeccion {
  APROBADA = "APROBADA",
  DECLINADA = "DECLINADA",
  FINALIZADA = "FINALIZADA",
  PENDIENTE = "PENDIENTE"
}

export enum EstadoVaramiento {
  PENDIENTE = "PENDIENTE",
  FINALIZADA = "FINALIZADA"
}



type TortugarioMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type EspecieTortugarioMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type EspecieMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type EspecieComprobanteMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type ComprobanteComercializacionMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type RecursoMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type HotelTortugarioMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type HotelMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type ColaboradorMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type TalonarioMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type PlayaMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type SectorMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type CatalogoMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type DonanteMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type EmergenciaMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type ExhumacionMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type ConceptoMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type InspeccionMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type PreguntaInspeccionMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type FotosInspeccionMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type VaramientoMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type FotosVaramientoMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type UsuarioMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type CompraHuevosMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type CompraDonacionJoinMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type DonacionEconomicaMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type DonacionHuevosMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type SeasonMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type NidoMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type NotificationQueueMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

export declare class Tortugario {
  readonly id: string;
  readonly nombre: string;
  readonly numeroRegistro?: string | null;
  readonly tipoTortugario: string;
  readonly direccionConap: string;
  readonly fechaInicioTemporada?: string | null;
  readonly fechaFinTemporada?: string | null;
  readonly administrarDonaciones: boolean;
  readonly latitud: number;
  readonly longitud: number;
  readonly direccion?: string | null;
  readonly huevosSembrados?: number | null;
  readonly neonatosLiberados?: number | null;
  readonly departamento?: string | null;
  readonly municipio?: string | null;
  readonly especies?: (EspecieTortugario | null)[] | null;
  readonly recursos?: (Recurso | null)[] | null;
  readonly hoteles?: (HotelTortugario | null)[] | null;
  readonly colaboradores?: (Colaborador | null)[] | null;
  readonly talonarios?: (Talonario | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Tortugario, TortugarioMetaData>);
  static copyOf(source: Tortugario, mutator: (draft: MutableModel<Tortugario, TortugarioMetaData>) => MutableModel<Tortugario, TortugarioMetaData> | void): Tortugario;
}

export declare class EspecieTortugario {
  readonly id: string;
  readonly tortugario: Tortugario;
  readonly especie: Especie;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<EspecieTortugario, EspecieTortugarioMetaData>);
  static copyOf(source: EspecieTortugario, mutator: (draft: MutableModel<EspecieTortugario, EspecieTortugarioMetaData>) => MutableModel<EspecieTortugario, EspecieTortugarioMetaData> | void): EspecieTortugario;
}

export declare class Especie {
  readonly id: string;
  readonly nombre: string;
  readonly descripcion?: string | null;
  readonly tortugario?: (EspecieTortugario | null)[] | null;
  readonly comprobante?: (EspecieComprobante | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Especie, EspecieMetaData>);
  static copyOf(source: Especie, mutator: (draft: MutableModel<Especie, EspecieMetaData>) => MutableModel<Especie, EspecieMetaData> | void): Especie;
}

export declare class EspecieComprobante {
  readonly id: string;
  readonly comprobante: ComprobanteComercializacion;
  readonly especie: Especie;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<EspecieComprobante, EspecieComprobanteMetaData>);
  static copyOf(source: EspecieComprobante, mutator: (draft: MutableModel<EspecieComprobante, EspecieComprobanteMetaData>) => MutableModel<EspecieComprobante, EspecieComprobanteMetaData> | void): EspecieComprobante;
}

export declare class ComprobanteComercializacion {
  readonly id: string;
  readonly talonarioID: string;
  readonly numeroComprobante: string;
  readonly fechaEmision: string;
  readonly horaEmision: string;
  readonly parlameroID: string;
  readonly cuotaConservacion: number;
  readonly playaID: string;
  readonly nombreAutorizo: string;
  readonly especies?: (EspecieComprobante | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<ComprobanteComercializacion, ComprobanteComercializacionMetaData>);
  static copyOf(source: ComprobanteComercializacion, mutator: (draft: MutableModel<ComprobanteComercializacion, ComprobanteComercializacionMetaData>) => MutableModel<ComprobanteComercializacion, ComprobanteComercializacionMetaData> | void): ComprobanteComercializacion;
}

export declare class Recurso {
  readonly id: string;
  readonly tortugarioID: string;
  readonly tipo: string;
  readonly archivoID: string;
  readonly bucket: string;
  readonly region: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Recurso, RecursoMetaData>);
  static copyOf(source: Recurso, mutator: (draft: MutableModel<Recurso, RecursoMetaData>) => MutableModel<Recurso, RecursoMetaData> | void): Recurso;
}

export declare class HotelTortugario {
  readonly id: string;
  readonly tortugario: Tortugario;
  readonly hotel: Hotel;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<HotelTortugario, HotelTortugarioMetaData>);
  static copyOf(source: HotelTortugario, mutator: (draft: MutableModel<HotelTortugario, HotelTortugarioMetaData>) => MutableModel<HotelTortugario, HotelTortugarioMetaData> | void): HotelTortugario;
}

export declare class Hotel {
  readonly id: string;
  readonly nombre: string;
  readonly link?: string | null;
  readonly descripcion?: string | null;
  readonly tortugario?: (HotelTortugario | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Hotel, HotelMetaData>);
  static copyOf(source: Hotel, mutator: (draft: MutableModel<Hotel, HotelMetaData>) => MutableModel<Hotel, HotelMetaData> | void): Hotel;
}

export declare class Colaborador {
  readonly id: string;
  readonly primerNombre: string;
  readonly segundoNombre?: string | null;
  readonly primerApellido: string;
  readonly segundoApellido?: string | null;
  readonly fechaNacimiento?: string | null;
  readonly sexo?: string | null;
  readonly lugarResidencia?: string | null;
  readonly telefono?: string | null;
  readonly correo?: string | null;
  readonly dpi?: string | null;
  readonly rol?: RolColaborador | keyof typeof RolColaborador | null;
  readonly tortugarioID: string;
  readonly archivoID?: string | null;
  readonly bucket: string;
  readonly region: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Colaborador, ColaboradorMetaData>);
  static copyOf(source: Colaborador, mutator: (draft: MutableModel<Colaborador, ColaboradorMetaData>) => MutableModel<Colaborador, ColaboradorMetaData> | void): Colaborador;
}

export declare class Talonario {
  readonly id: string;
  readonly noTalonario: number;
  readonly noInicioComprobante: string;
  readonly noFinComprobante: string;
  readonly tortugarioID: string;
  readonly comprobantes?: (ComprobanteComercializacion | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Talonario, TalonarioMetaData>);
  static copyOf(source: Talonario, mutator: (draft: MutableModel<Talonario, TalonarioMetaData>) => MutableModel<Talonario, TalonarioMetaData> | void): Talonario;
}

export declare class Playa {
  readonly id: string;
  readonly nombre: string;
  readonly descripcion?: string | null;
  readonly sector?: (Sector | null)[] | null;
  readonly delete?: boolean | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Playa, PlayaMetaData>);
  static copyOf(source: Playa, mutator: (draft: MutableModel<Playa, PlayaMetaData>) => MutableModel<Playa, PlayaMetaData> | void): Playa;
}

export declare class Sector {
  readonly id: string;
  readonly playaID: string;
  readonly nombre: string;
  readonly descripcion?: string | null;
  readonly delete?: boolean | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Sector, SectorMetaData>);
  static copyOf(source: Sector, mutator: (draft: MutableModel<Sector, SectorMetaData>) => MutableModel<Sector, SectorMetaData> | void): Sector;
}

export declare class Catalogo {
  readonly id: string;
  readonly catalogo: string;
  readonly codigo: string;
  readonly descripcion: string;
  readonly order?: number | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Catalogo, CatalogoMetaData>);
  static copyOf(source: Catalogo, mutator: (draft: MutableModel<Catalogo, CatalogoMetaData>) => MutableModel<Catalogo, CatalogoMetaData> | void): Catalogo;
}

export declare class Donante {
  readonly id: string;
  readonly primerNombre: string;
  readonly segundoNombre?: string | null;
  readonly primerApellido: string;
  readonly segundoApellido?: string | null;
  readonly genero: string;
  readonly pais: string;
  readonly ciudad: string;
  readonly telefono?: string | null;
  readonly correo?: string | null;
  readonly usaWhatsapp?: boolean | null;
  readonly tipoDonanate: string;
  readonly categoriaDonanate: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Donante, DonanteMetaData>);
  static copyOf(source: Donante, mutator: (draft: MutableModel<Donante, DonanteMetaData>) => MutableModel<Donante, DonanteMetaData> | void): Donante;
}

export declare class Emergencia {
  readonly id: string;
  readonly tortugarioID: string;
  readonly fechaEmergencia: string;
  readonly neonatosVivos: number;
  readonly neonatosMuertos: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Emergencia, EmergenciaMetaData>);
  static copyOf(source: Emergencia, mutator: (draft: MutableModel<Emergencia, EmergenciaMetaData>) => MutableModel<Emergencia, EmergenciaMetaData> | void): Emergencia;
}

export declare class Exhumacion {
  readonly id: string;
  readonly tortugarioID: string;
  readonly fechaExhumacion: string;
  readonly neonatosVivos: number;
  readonly neonatosMuertos: number;
  readonly huevosNoEclosionados: number;
  readonly huevosInfertiles: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Exhumacion, ExhumacionMetaData>);
  static copyOf(source: Exhumacion, mutator: (draft: MutableModel<Exhumacion, ExhumacionMetaData>) => MutableModel<Exhumacion, ExhumacionMetaData> | void): Exhumacion;
}

export declare class Concepto {
  readonly id: string;
  readonly nombre: string;
  readonly descripcion?: string | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Concepto, ConceptoMetaData>);
  static copyOf(source: Concepto, mutator: (draft: MutableModel<Concepto, ConceptoMetaData>) => MutableModel<Concepto, ConceptoMetaData> | void): Concepto;
}

export declare class Inspeccion {
  readonly id: string;
  readonly fecha?: string | null;
  readonly horaInicio?: string | null;
  readonly horaFin?: string | null;
  readonly comprobante?: string | null;
  readonly direccionRegional?: string | null;
  readonly torguatioID?: string | null;
  readonly recomendaciones?: string | null;
  readonly motivoDenegacion?: string | null;
  readonly estado?: EstadoInspeccion | keyof typeof EstadoInspeccion | null;
  readonly preguntas?: (PreguntaInspeccion | null)[] | null;
  readonly fotos?: (FotosInspeccion | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Inspeccion, InspeccionMetaData>);
  static copyOf(source: Inspeccion, mutator: (draft: MutableModel<Inspeccion, InspeccionMetaData>) => MutableModel<Inspeccion, InspeccionMetaData> | void): Inspeccion;
}

export declare class PreguntaInspeccion {
  readonly id: string;
  readonly inspeccionID: string;
  readonly numero: number;
  readonly pregunta: string;
  readonly respuesta: string;
  readonly seccion: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<PreguntaInspeccion, PreguntaInspeccionMetaData>);
  static copyOf(source: PreguntaInspeccion, mutator: (draft: MutableModel<PreguntaInspeccion, PreguntaInspeccionMetaData>) => MutableModel<PreguntaInspeccion, PreguntaInspeccionMetaData> | void): PreguntaInspeccion;
}

export declare class FotosInspeccion {
  readonly id: string;
  readonly inspeccionID: string;
  readonly tipo: string;
  readonly archivoID: string;
  readonly bucket: string;
  readonly region: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<FotosInspeccion, FotosInspeccionMetaData>);
  static copyOf(source: FotosInspeccion, mutator: (draft: MutableModel<FotosInspeccion, FotosInspeccionMetaData>) => MutableModel<FotosInspeccion, FotosInspeccionMetaData> | void): FotosInspeccion;
}

export declare class Varamiento {
  readonly id: string;
  readonly colaboradorID: string;
  readonly tortugarioID: string;
  readonly estadoTortuga?: string | null;
  readonly fecha?: string | null;
  readonly hora?: string | null;
  readonly indicadorMarca?: string | null;
  readonly informacionMarchamo?: string | null;
  readonly aletaMarchamo?: string | null;
  readonly especieID?: string | null;
  readonly edadTortuga?: string | null;
  readonly sexo?: string | null;
  readonly playaID?: string | null;
  readonly sectorID?: string | null;
  readonly latitud?: number | null;
  readonly longitud?: number | null;
  readonly largoCaparazon?: number | null;
  readonly anchoCaparazon?: number | null;
  readonly condicionClimatica?: string | null;
  readonly indicadorColectaTejido?: string | null;
  readonly indicadorBarcos?: string | null;
  readonly lesionesEncontradas?: (string | null)[] | null;
  readonly estado?: EstadoVaramiento | keyof typeof EstadoVaramiento | null;
  readonly fotos?: (FotosVaramiento | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Varamiento, VaramientoMetaData>);
  static copyOf(source: Varamiento, mutator: (draft: MutableModel<Varamiento, VaramientoMetaData>) => MutableModel<Varamiento, VaramientoMetaData> | void): Varamiento;
}

export declare class FotosVaramiento {
  readonly id: string;
  readonly varamientoID: string;
  readonly tipo: string;
  readonly archivoID: string;
  readonly bucket: string;
  readonly region: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<FotosVaramiento, FotosVaramientoMetaData>);
  static copyOf(source: FotosVaramiento, mutator: (draft: MutableModel<FotosVaramiento, FotosVaramientoMetaData>) => MutableModel<FotosVaramiento, FotosVaramientoMetaData> | void): FotosVaramiento;
}

export declare class Usuario {
  readonly id: string;
  readonly username: string;
  readonly password: string;
  readonly email?: string | null;
  readonly rol: string;
  readonly tortugarioId?: string | null;
  readonly confirmado?: boolean | null;
  readonly delete?: boolean | null;
  readonly telefono?: string | null;
  readonly archivoID?: string | null;
  readonly primerNombre: string;
  readonly segundoNombre?: string | null;
  readonly primerApellido: string;
  readonly segundoApellido?: string | null;
  readonly sexo: string;
  readonly lugarResidencia: string;
  readonly fechaNacimiento?: string | null;
  readonly noConap?: string | null;
  readonly dpi?: string | null;
  readonly pais?: string | null;
  readonly tipoDonante?: string | null;
  readonly comprobantes?: (ComprobanteComercializacion | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Usuario, UsuarioMetaData>);
  static copyOf(source: Usuario, mutator: (draft: MutableModel<Usuario, UsuarioMetaData>) => MutableModel<Usuario, UsuarioMetaData> | void): Usuario;
}

export declare class CompraHuevos {
  readonly id: string;
  readonly userID: string;
  readonly tortugarioID: string;
  readonly fecha: string;
  readonly cantidadHuevos: number;
  readonly cantidadHuevosDisponibles?: number | null;
  readonly monto: number;
  readonly especieProbable: string;
  readonly sectorID: string;
  readonly playaID: string;
  readonly donaciones?: (CompraDonacionJoin | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<CompraHuevos, CompraHuevosMetaData>);
  static copyOf(source: CompraHuevos, mutator: (draft: MutableModel<CompraHuevos, CompraHuevosMetaData>) => MutableModel<CompraHuevos, CompraHuevosMetaData> | void): CompraHuevos;
}

export declare class CompraDonacionJoin {
  readonly id: string;
  readonly donacionEconomica: DonacionEconomica;
  readonly CompraHuevos: CompraHuevos;
  readonly monto: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<CompraDonacionJoin, CompraDonacionJoinMetaData>);
  static copyOf(source: CompraDonacionJoin, mutator: (draft: MutableModel<CompraDonacionJoin, CompraDonacionJoinMetaData>) => MutableModel<CompraDonacionJoin, CompraDonacionJoinMetaData> | void): CompraDonacionJoin;
}

export declare class DonacionEconomica {
  readonly id: string;
  readonly donanteID: string;
  readonly tortugarioID: string;
  readonly fecha: string;
  readonly monto: number;
  readonly montoUtilizado?: number | null;
  readonly token: string;
  readonly status?: string | null;
  readonly compras?: (CompraDonacionJoin | null)[] | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<DonacionEconomica, DonacionEconomicaMetaData>);
  static copyOf(source: DonacionEconomica, mutator: (draft: MutableModel<DonacionEconomica, DonacionEconomicaMetaData>) => MutableModel<DonacionEconomica, DonacionEconomicaMetaData> | void): DonacionEconomica;
}

export declare class DonacionHuevos {
  readonly id: string;
  readonly donanteID: string;
  readonly tortugarioID: string;
  readonly fecha: string;
  readonly cantidadHuevos: number;
  readonly especieProbable: string;
  readonly token: string;
  readonly status?: string | null;
  readonly sectorID: string;
  readonly playaID: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<DonacionHuevos, DonacionHuevosMetaData>);
  static copyOf(source: DonacionHuevos, mutator: (draft: MutableModel<DonacionHuevos, DonacionHuevosMetaData>) => MutableModel<DonacionHuevos, DonacionHuevosMetaData> | void): DonacionHuevos;
}

export declare class Season {
  readonly id: string;
  readonly start: string;
  readonly end: string;
  readonly status: boolean;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Season, SeasonMetaData>);
  static copyOf(source: Season, mutator: (draft: MutableModel<Season, SeasonMetaData>) => MutableModel<Season, SeasonMetaData> | void): Season;
}

export declare class Nido {
  readonly id: string;
  readonly tortugarioID: string;
  readonly nido: number;
  readonly fecha: string;
  readonly huevos: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Nido, NidoMetaData>);
  static copyOf(source: Nido, mutator: (draft: MutableModel<Nido, NidoMetaData>) => MutableModel<Nido, NidoMetaData> | void): Nido;
}

export declare class NotificationQueue {
  readonly id: string;
  readonly type: string;
  readonly recipients: string[];
  readonly body: string;
  readonly subject: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<NotificationQueue, NotificationQueueMetaData>);
  static copyOf(source: NotificationQueue, mutator: (draft: MutableModel<NotificationQueue, NotificationQueueMetaData>) => MutableModel<NotificationQueue, NotificationQueueMetaData> | void): NotificationQueue;
}