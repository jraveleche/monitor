// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';

const RolColaborador = {
  "ADMINISTRADOR": "ADMINISTRADOR",
  "ENCARGADO": "ENCARGADO",
  "AYUDANTE": "AYUDANTE",
  "PROMOTOR": "PROMOTOR"
};

const EstadoInspeccion = {
  "APROBADA": "APROBADA",
  "DECLINADA": "DECLINADA",
  "FINALIZADA": "FINALIZADA",
  "PENDIENTE": "PENDIENTE"
};

const EstadoVaramiento = {
  "PENDIENTE": "PENDIENTE",
  "FINALIZADA": "FINALIZADA"
};

const { Tortugario, EspecieTortugario, Especie, EspecieComprobante, ComprobanteComercializacion, Recurso, HotelTortugario, Hotel, Colaborador, Talonario, Playa, Sector, Catalogo, Donante, Emergencia, Exhumacion, Concepto, Inspeccion, PreguntaInspeccion, FotosInspeccion, Varamiento, FotosVaramiento, Usuario, CompraHuevos, CompraDonacionJoin, DonacionEconomica, DonacionHuevos, Season, Nido, NotificationQueue } = initSchema(schema);

export {
  Tortugario,
  EspecieTortugario,
  Especie,
  EspecieComprobante,
  ComprobanteComercializacion,
  Recurso,
  HotelTortugario,
  Hotel,
  Colaborador,
  Talonario,
  Playa,
  Sector,
  Catalogo,
  Donante,
  Emergencia,
  Exhumacion,
  Concepto,
  Inspeccion,
  PreguntaInspeccion,
  FotosInspeccion,
  Varamiento,
  FotosVaramiento,
  Usuario,
  CompraHuevos,
  CompraDonacionJoin,
  DonacionEconomica,
  DonacionHuevos,
  Season,
  Nido,
  NotificationQueue,
  RolColaborador,
  EstadoInspeccion,
  EstadoVaramiento
};