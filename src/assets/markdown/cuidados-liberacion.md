Si asiste a una liberación de neonatos, tome en cuenta las siguientes recomendaciones:
- ¡NO los toque, NO los tome con la mano! Así evitará contaminarlos y transmitirles enfermedades. 
- Ubíquense detrás del cordón de seguridad que coloca el encargado del tortugario.
- Evite tomar fotografías con flash pues las tortuguitas se desorientan.
- Mientras las tortugas van hacia el mar, los observadores deberán permanecer en silencio.
