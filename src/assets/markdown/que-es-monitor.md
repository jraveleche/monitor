<span style="color: #9bc682; font-weight: bold;">moniTOR</span> es una aplicación sin fines de lucro que surgió como una iniciativa para mejorar el monitoreo y conservación de las tortugas marinas en Guatemala por medio del desarrollo y la utilización de una solución informática. Tiene como objetivos específicos: 

- Facilitar y estandardizar la recolección de datos de los tortugarios y la generación de informes para el Consejo Nacional de Áreas Protegidas de Guatemala (CONAP). 
- Facilitar al CONAP la consolidación de los datos de la conservación de las tortugas marinas a nivel de país y su reporte a la Convención Interamericana para la Protección y Conservación de las Tortugas Marinas (CIT).
- Poner los datos de la conservación de las tortugas marinas a disposición de la academia y otras partes interesadas para su análisis.
- Divulgar al público en general la información del monitoreo y conservación de las tortugas marinas de Guatemala. 
- Registrar datos relacionados con contribuciones privadas a los tortugarios para la rendición de cuentas al donante. 

<span style="color: #9bc682; font-weight: bold;">moniTOR</span> consta de una aplicación en versión móvil (APP móvil) de Android que es utilizada por los administradores y encargados de los tortugarios por medio de sus celulares y que permite la recolección de los datos a nivel local, así como por el personal supervisor de los tortugarios de CONAP.
