Los tortugarios guatemaltecos dependen de fondos del sector privado ya que el CONAP carece de los recursos y personal para administrarlos por si solo.

__Si usted quiere apoyar la conservación de las tortugas marinas y donar para la compra de huevos, haga clic [aquí](#/listado/tortugarios), escoja un tortugario y póngase en contacto con el tortugario que le interesa.__

El tortugario le indicará la forma en que trabaja para la recepción de fondos, ya sea por medio de transferencias bancarias, con tarjetas de crédito o débito, en efectivo o a través de una página web.

Pero no solo puede apoyar con dinero, también puede poner en práctica las siguientes recomendaciones: 
- No compre huevos de tortuga marina para consumo. 
- Si encuentra casualmente un nido, entregue los huevos al tortugario de la localidad.
- Si ve a una tortuga saliendo del mar o anidando:
    - Manténgase en silencio y a una distancia mínima de cinco metros
    - No la moleste, no la toque, no fume en este momento, no la alumbre, ni tome fotos con flash. 
- Si asiste a una liberación de neonatos no los tome con la mano y evite tomar fotografías con flash pues las tortuguitas se desorientan.
- Ayude a mantener limpias las playas para que las tortugas que llegan a anidar no encuentren obstáculos. Si va a una playa deposite la basura en su lugar o lleve sus residuos de regreso a casa. 
- La época de anidación es de julio a enero. Evite que sus animales domésticos destruyan los nidos de tortugas marinas.
- Evite el uso de vehículos y motocicletas en las playas, principalmente durante las noches.
- Si encuentra una tortuga viva boca arriba, pida ayuda para voltearla pues el peso del resto de sus órganos internos y de ella misma aplastará sus pulmones.
