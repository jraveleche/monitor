import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Auth } from "aws-amplify";
import { Observable } from "rxjs";

export class AuthGuard implements CanActivate {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.isValidSession();
    }

    private async isValidSession(): Promise<boolean> {
        return new Promise<boolean>(async resolve => {
            try {
                let result = await Auth.currentSession();
                resolve(result.isValid());
            }catch(error){
                console.log("No se pudo obtener la sesión actual ", error);
                resolve(false);
            }
        })
    }
}
