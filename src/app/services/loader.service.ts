import { Injectable } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  @BlockUI() private blockUI!: NgBlockUI;
  
  constructor() { }

  show(){
    this.blockUI.start();
  }

  hide(){
    this.blockUI.reset();
  }
}
