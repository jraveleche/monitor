import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fsServer from 'file-saver';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor() { }

  async donwloadFile(headers : string [] , data : any [] , nameFile: string, type? : string) {
   
    let dataExcel = [];
    data.forEach((value, index) => {
      let row = [ (index +1)];
      row = row.concat(Object.values(value));
      dataExcel.push(row);
    });

    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('varamientos');
    worksheet.addRow(headers);
    dataExcel.forEach(value => {
      worksheet.addRow(value);
    });

    
    if(type === 'csv') {
      workbook.csv.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'text/csv' });
        fsServer.saveAs(blob, nameFile);
      });
    }else {
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fsServer.saveAs(blob, nameFile);
      });
    }

  }

}
