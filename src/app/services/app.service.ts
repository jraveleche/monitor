import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private monitorUrl = environment.monitorApi;

  constructor(private http : HttpClient) { }

 async sendEmail(emailRequest: any) : Promise<any>{

    return this.http.post(`${this.monitorUrl}email`, emailRequest)
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(e => {
        return {
          status: 500, 
          error: e,
        }
      })

  }
}
