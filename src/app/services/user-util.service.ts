import { Injectable } from '@angular/core';
import { Auth, DataStore } from 'aws-amplify';
import { Usuario } from 'src/models';

@Injectable({
  providedIn: 'root',
})
export class UserUtilService {
  constructor() {}

  async createUser(user: Usuario) {
    try {
      let userModel = new Usuario({ ...user });
      await Auth.signUp({
        username: userModel.username,
        password: userModel.password,
        attributes: {
          email: userModel.email,
        },
      });
      await DataStore.save(userModel);
    } catch (error) {
      console.error('Error al registrar usuario', error);
    }
  }

  async signIn(username: string, password: string) {
    try {
      await Auth.signIn(username, password);
      return true;
    } catch (error) {
      console.error('Error al autenticar usuario ', error);
      return false;
    }
  }

  async confimUser(user: string, code: string) {
    try {
      let result = await Auth.confirmSignUp(user, code);
      if (result) {
        return true;
      }
      return false;
    } catch (error) {
      console.log('No se pudo confirmar el usuario', error);
      return false;
    }
  }

  async logOut() {
    try {
      await Auth.signOut({ global: true });
    } catch (error) {
      console.log('Error al cerrar la sesisón del usuario', error);
    }
  }

  async getCurrentSession() {
    try {
      return await Auth.currentSession();
    }catch(error){
      console.log("Hubo un error al obtener la sesión", error);
      return '';
    }
    
  }
}
