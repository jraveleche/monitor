import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from "rxjs/operators";
import { Router } from '@angular/router';
import { LoaderService } from '../loader.service';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  private activeRequest = 0;
  constructor(readonly loaderService: LoaderService, readonly route: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loaderService.show();
    
    return next.handle(request).pipe(
      finalize(() => {
        this.activeRequest--;
        if(this.activeRequest <= 0){
            this.loaderService.hide();
        }
    }),
    catchError(err => {
        this.activeRequest = 0;
        this.loaderService.hide();
        return throwError(err);
      })
    );
  }

}
