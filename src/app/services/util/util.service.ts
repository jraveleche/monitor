import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Subject } from 'rxjs';
import { InformationDialogComponent } from 'src/app/components/dialogs/information-dialog/information-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  subject = new BehaviorSubject<boolean>(false);
  subjectFooter = new BehaviorSubject<any>('');
  constructor(private snackBar : MatSnackBar, private dialog: MatDialog) { }

  hide(show : boolean) {
    this.subject.next(show);
  }

  goToSection(section: string, isPrincipal?: boolean){
    this.subjectFooter.next({
      section: section,
      isPrincipal: isPrincipal
    });
  }

  hideComponenet(){
    return this.subject.asObservable();
  }

  renderSection() {
    return this.subjectFooter.asObservable();
  }

  openSnackBar(message: string, action: string, duration? : number) {
    if(!duration){
      duration = 3000;
    }
    this.snackBar.open(message, action, {
      duration: duration
    });
  }


  showInformationDialog(data: any, height?: number) {
    this.dialog.open(InformationDialogComponent , {
      disableClose: true,
      panelClass: 'custom-container',
      scrollStrategy: new NoopScrollStrategy(),
      data: data
    })
  }
}
