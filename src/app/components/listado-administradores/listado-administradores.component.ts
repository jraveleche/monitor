import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataStore } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { Usuario } from 'src/models';

@Component({
  selector: 'app-listado-administradores',
  templateUrl: './listado-administradores.component.html',
  styleUrls: ['./listado-administradores.component.scss']
})
export class ListadoAdministradoresComponent implements OnInit {

  dataSource: MatTableDataSource<Usuario>;
  displayedColumns: string[] = ['no', 'nombre', 'email', 'telefono'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private loaderService: LoaderService, private dialog : MatDialog) { }

  ngOnInit(): void {
    this.getAdminUser();
  }

  async getAdminUser() {
    this.loaderService.show();
    let usuarios = await DataStore.query(Usuario, u => u.rol('eq', 'ADMINISTRADOR'));
    this.dataSource = new MatTableDataSource(usuarios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loaderService.hide();
  }

  getName(usuario : Usuario) : string {
    let names = `${usuario.primerNombre} ${usuario.segundoNombre}`;
    let lastnames = `${usuario.primerApellido} ${usuario.segundoApellido}`;

    return `${names} ${lastnames}`.replace(/\s+/, " ").trim();
  }

}
