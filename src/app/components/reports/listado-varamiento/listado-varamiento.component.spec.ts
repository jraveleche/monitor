import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoVaramientoComponent } from './listado-varamiento.component';

describe('ListadoVaramientoComponent', () => {
  let component: ListadoVaramientoComponent;
  let fixture: ComponentFixture<ListadoVaramientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoVaramientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoVaramientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
