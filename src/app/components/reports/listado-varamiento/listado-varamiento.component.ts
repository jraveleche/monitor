import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataStore, SortDirection } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { Especie, Playa, Sector, Tortugario, Varamiento } from 'src/models';
import { FileService } from 'src/app/services/file/file.service';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-listado-varamiento',
  templateUrl: './listado-varamiento.component.html',
  styleUrls: ['./listado-varamiento.component.scss']
})
export class ListadoVaramientoComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  data :  any [] = [];
  displayedColumns: string[] = [
    'no',  
    'tortugario', 
    'especie', 
    'edadTortuga', 
    'sexo',
    'estadoTortuga',
    'reporte'
  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  headersReport = [
    'No.',
    'Id',
    'Nombre tortugario',
    'Nombre especie', 
    'Edad tortuga',
    'Sexo',
    'Fecha varamiento',
    'Hora varamiento',
    'Longitud',
    'Latitud',
    'Nombre playa',
    'Sector playa',
    'Indicador marca',
    'Información marchamo',
    'Aleta marchamo',
    'Largo caparazón',
    'Ancho caparazón',
    'Condición climática',
    'Indicador colecta tejido',
    'Incador barcos',
    'Lesiones encontradas',
    'Estado tortuga'
  ]

  constructor(
    private loaderService: LoaderService, 
    private dialog : MatDialog,
    private fileService: FileService) { }

  ngOnInit(): void {
    this.getVaramientos();
  }

  async getVaramientos() {
    this.loaderService.show();
    let varamientos = await DataStore.query(Varamiento, v => v.estado('eq', 'FINALIZADA'), {
      sort: e =>  e.fecha(SortDirection.DESCENDING)
    });
    this.data =  await Promise.all(varamientos.map( async v => {
      let especie = await DataStore.query(Especie, v.especieID);
      let tortugario = await DataStore.query(Tortugario, v.tortugarioID);
      let playa = await DataStore.query(Playa, v.playaID);
      let sector = await DataStore.query(Sector, v.sectorID);
      return {
        id:  v.id,
        tortugario: tortugario.nombre,
        especie: especie ? especie.nombre : 'NR',
        edadTortuga: v.edadTortuga ? v.edadTortuga : 'NR',
        sexo: v.sexo? v.sexo : 'NR',
        fecha: v.fecha,
        hora: v.hora,
        longitud: v.longitud,
        latitud: v.latitud,
        playa: playa ? playa.nombre : 'NR',
        sector: sector ? sector.nombre : 'NR',
        indicadorMarca: v.indicadorMarca ? v.indicadorMarca : 'NO',
        informacionMarchamo: v.informacionMarchamo ? v.informacionMarchamo : 'NR',
        aletaMarchamo: v.aletaMarchamo ? v.aletaMarchamo : 'NR',
        largoCaparazon: v.largoCaparazon ? v.largoCaparazon : 'NR',
        anchoCaparazon: v.anchoCaparazon ? v.anchoCaparazon : 'NR',
        condicionClimatica: v.condicionClimatica ? v.condicionClimatica : 'NR',
        indicadorColectaTejido: v.indicadorColectaTejido ? v.indicadorColectaTejido : 'NO',
        indicadorBarcos: v.indicadorBarcos ? v.indicadorBarcos : 'NO',
        lesionesEncontradas: v.lesionesEncontradas ? v.lesionesEncontradas.join('|') : '' ,
        estadoTortuga:  v.estadoTortuga ? v.estadoTortuga : 'NR'
      }
    }));
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loaderService.hide();
  }

  downloadExcel() {
    this.loaderService.show();
    let nameFile = `varamientos-${moment(new Date()).format('DD-MM-yyyy')}.xlsx`;
    this.fileService.donwloadFile(this.headersReport, this.data, nameFile);
    this.loaderService.hide();
  }

  downloadCsv() {
    this.loaderService.show();
    let nameFile = `varamientos-${moment(new Date()).format('DD-MM-yyyy')}.csv`;
    this.fileService.donwloadFile(this.headersReport, this.data, nameFile, 'csv');
    this.loaderService.hide();
  }

  openPdf(id : string) {
    window.open(`${environment.monitorApi}varamientos/${id}/report`, '_blank');
  }

}
