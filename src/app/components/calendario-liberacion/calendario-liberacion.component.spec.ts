import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarioLiberacionComponent } from './calendario-liberacion.component';

describe('CalendarioLiberacionComponent', () => {
  let component: CalendarioLiberacionComponent;
  let fixture: ComponentFixture<CalendarioLiberacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalendarioLiberacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarioLiberacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
