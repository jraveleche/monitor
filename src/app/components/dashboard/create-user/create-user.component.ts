import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataStore } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { UserUtilService } from 'src/app/services/user-util.service';
import { UtilService } from 'src/app/services/util/util.service';
import { Usuario } from 'src/models';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})

export class CreateUserComponent implements OnInit {


  userForm: FormGroup;
  hidePassword = true;
  @Output("onHideComponent") hide = new EventEmitter<any>();


  constructor(
    private formBuilder : FormBuilder,
    private util: UtilService,
    private loaderService: LoaderService,
    private userUtil: UserUtilService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required , Validators.email])],
      username: ['', Validators.compose([Validators.required , Validators.pattern('^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$')])],
      password: ['', Validators.compose([Validators.required , Validators.minLength(6)])],
      phone: ['', Validators.compose([Validators.required , Validators.pattern('^[0-9]{8}$')])],
      rol: ['', Validators.required],
    })
  }

  confirmCreation() {

    if(this.userForm.invalid) {
      this.util.openSnackBar('Por favor verifica los campos para poder continuar', '¡Notificación!');
      return;
    }

    Swal.fire({
      icon: 'question',
      title: 'Crear nuevo usuario',
      html: `Deseas crear a <strong>${this.userForm.controls.username.value}</strong> ¿cómo un nuevo usuario en la plataforma?`,
      showDenyButton: true,
      focusDeny: true,
      confirmButtonColor: '#0c86cd',
      confirmButtonText: 'Crear',
      denyButtonText: 'Cancelar',
      allowOutsideClick: false, 
      allowEscapeKey: false,
    }).then(result => {
      if(result.isConfirmed) {
        this.createUser();
      }
    });

  }

  private async createUser() {
    this.loaderService.show();
    let users = await DataStore.query(Usuario, u => u.username('eq', this.userForm.controls.username.value));
    if(users.length > 0) {
      this.loaderService.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        html: `El usuario <strong>${this.userForm.controls.username.value}</strong> ya existe en la plataforma, por favor cambia su nombre de usuario`,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
      });
      return;
    }

    try {
      let user = new Usuario({
        username : this.userForm.controls.username.value,
        password: this.userForm.controls.password.value,
        rol: this.userForm.controls.rol.value,
        confirmado: false, 
        telefono: `+502${this.userForm.controls.phone.value}`,
        email: this.userForm.controls.email.value,
        primerNombre: '',
        primerApellido: '',
        sexo: '',
        fechaNacimiento: '',
        lugarResidencia: ''
      });
      await DataStore.save(user);
      await this.userUtil.createUser(user);
      this.loaderService.hide();
      Swal.fire({
        icon: 'success',
        title: 'Nuevo usuario creado',
        html: `Se ha registrado la información correctamente`,
        focusConfirm: true,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
        allowOutsideClick: false,
        allowEscapeKey: false,
      }).then(result => {
        if(result.isConfirmed) {
          this.router.navigate(['menu']);
        }
      });
    }catch(error) {
      console.error('Hubo un error en la creación', error);
      this.loaderService.hide();
    }
  }

  cancel() {
    this.hide.emit(
      {
        show: true,
        section: 'home'
      });
  }


  errorMessage(typeError : string, control : AbstractControl) {
    
    const helperErrorMessage: { [key: string] : string} = {
      email: 'Por favor ingrese un correo válido.',
      username : 'Por favor ingrese un nombre de usuario válido.',
      password : 'Por favor ingrese una contraseña de mínimo 6 caracteres',
      confirmPassword : 'La confirmación no coincide con la contraseña',
      phone : 'Por favor ingrese un número de teléfono válido.',
      rol : 'Por favor seleciona un rol',
    };

    if(control.invalid && control.touched){
      let message = helperErrorMessage[typeError] || "";
      if(message){
        this.util.openSnackBar(message, "¡Errro!");
      }
    }

  }

}
