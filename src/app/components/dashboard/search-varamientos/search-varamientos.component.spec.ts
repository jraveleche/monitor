import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchVaramientosComponent } from './search-varamientos.component';

describe('SearchVaramientosComponent', () => {
  let component: SearchVaramientosComponent;
  let fixture: ComponentFixture<SearchVaramientosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchVaramientosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchVaramientosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
