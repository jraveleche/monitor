import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataStore } from 'aws-amplify';
import { Colaborador, Especie, Tortugario, Varamiento } from 'src/models';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-search-varamientos',
  templateUrl: './search-varamientos.component.html',
  styleUrls: ['./search-varamientos.component.scss']
})
export class SearchVaramientosComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  @Input("tortugarioId") tortugarioId : string;
  @Input("nombreTortugario") nombreTortugario: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['fecha', 'hora', 'nombreColaborador', 'nombreEspecie', 'nombreTortugario', 'estado', 'acciones'];
  title: string;
  showTable: boolean =  false;

  constructor() { }

  ngOnInit(): void {
    this.getListVaramientos();
  }


  async getListVaramientos() {
    let varameintos = await DataStore.query(Varamiento, v => v.tortugarioID('eq', this.tortugarioId));
    this.showTable = varameintos.length > 0;
    let result = await Promise.all(varameintos.map(async e =>{
      let tortugario = await DataStore.query(Tortugario, e.tortugarioID);
      let colaborador = await DataStore.query(Colaborador, e.colaboradorID);
      let especie = await DataStore.query(Especie, e.especieID);
      let completName = this.getColaboradorName(colaborador);
      let objectResult = {...e};
      objectResult['nombreTortugario'] = tortugario.nombre.toUpperCase();
      objectResult['nombreColaborador'] = completName;
      objectResult['nombreEspecie'] = especie && especie.nombre ?especie.nombre.toUpperCase() : '';
      return objectResult;

    }));
    this.dataSource = new MatTableDataSource(result);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private getColaboradorName(colaborador : Colaborador) {
    if(!colaborador){
      return "";
    }

    let names = `${colaborador.primerNombre ? colaborador.primerNombre : ''} ${colaborador.segundoNombre ? colaborador.segundoNombre : ''}`;
    let lastnames = `${colaborador.primerApellido ? colaborador.primerApellido : ''} ${colaborador.segundoApellido ? colaborador.segundoApellido : ''}`;
    let completName = `${names} ${lastnames}`;
    completName = completName.replace(/\s+/, ' ');
    completName = completName.toUpperCase();
    return completName.trim();
  }

  applyFilter(event: Event) {
    console.log("buscando");
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openPdf(id : string){
    window.open(`${environment.monitorApi}varamientos/${id}/report`, '_blank');
  }

}
