import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoaderService } from 'src/app/services/loader.service';
import { UserUtilService } from 'src/app/services/user-util.service';
import { UtilService } from 'src/app/services/util/util.service';
import { Usuario } from 'src/models';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({opacity:0}),
        animate(500, style({opacity:1})) 
      ]),
      transition(':leave', [
        animate(500, style({opacity:0})) 
      ])
    ])
  ]
})
export class MenuComponent implements OnInit, OnDestroy {

  user: Usuario;

  options : any = [
    [
      {
        title: 'Creación de tortugario',
        subtitle: 'Creación',
        image : 'assets/img/Tortugario.png',
        description: 'Modulo para creación de tortugarios privados y públicos',
        section: 'createTortugario',
        show: false,
        rol: ['all']
      },
      {
        title: 'Generación de reportes',
        subtitle: 'Reporteia',
        image : 'assets/img/reportes.png',
        description: '',
        section: 'reports',
        show: false,
        rol: ['all']
      },
      {
        title: 'Creación de usuairos',
        subtitle: 'CONAP y Administradores',
        image : 'assets/img/Colaboradores.png',
        description: '',
        section: 'crearUsuario',
        show: true,
        rol: ['root']
      }
    ]
  ];

  reports : any = [
    [
      {
        title: 'Boeleta tortuga marina varada',
        subtitle: 'Reporte',
        image : 'assets/img/report-icon.png',
        description: '',
        section: 'reporteVaramiento',
        show: false
      },
      {
        title: 'Inspección tortugario',
        subtitle: 'Reporte',
        image : 'assets/img/report-icon.png',
        description: '',
        section: 'reporteInspeccion',
        show: false
      },
      {
        title: 'Boleta de datos mensuales',
        subtitle: 'Reporte',
        image : 'assets/img/report-icon.png',
        description: '',
        section: 'reporteDatosMensuales',
        show: false
      }
    ]
  ];

  sections : any  = {};
  constructor(
    private util: UtilService,
    private userUtil: UserUtilService,
    private loaderService: LoaderService,
    private router: Router
  ) { }

  ngOnInit(): void {
    let userJson = sessionStorage.getItem('actual-user');
    if(userJson){
      this.user = JSON.parse(userJson);
    }
    this.util.hide(true);
    this.addClassMenu();
    this.sections = {
      show: true,
      section: 'home'
    };
  }

  ngOnDestroy(){
    this.util.hide(false);
  }

  private addClassMenu(){
    document.addEventListener("DOMContentLoaded", function (event) {
      const showNavbar = (toggleId: any , navId: any, bodyId : any, headerId: any) => {
        const toggle = document.getElementById(toggleId),
          nav = document.getElementById(navId),
          bodypd = document.getElementById(bodyId),
          headerpd = document.getElementById(headerId)

        if (toggle && nav && bodypd && headerpd) {
          toggle.addEventListener('click', () => {
            nav.classList.toggle('show')
            toggle.classList.toggle('bx-x')
            bodypd.classList.toggle('body-pd')
            headerpd.classList.toggle('body-pd')
          });
        }
      }

      showNavbar('header-toggle', 'nav-bar', 'body-pd', 'header')
      const linkColor = document.querySelectorAll('.nav_link')

      function colorLink() {
        if (linkColor) {
          linkColor.forEach(l => l.classList.remove('active'))
          this.classList.add('active')
        }
      }
      linkColor.forEach(l => l.addEventListener('click', colorLink));
    });
  }

  showComponent(id : string){
    this.sections = {};
    this.sections = {
      show: true,
      section: id
    };
  }

  onHide(event: any){
    this.sections = event;
  }

  async logOut(){
    this.loaderService.show();
    await this.userUtil.logOut();
    this.loaderService.hide();
    sessionStorage.clear();
    this.router.navigate(['']);
  }

}
