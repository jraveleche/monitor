import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TortugarioComponent } from './tortugario.component';

describe('TortugarioComponent', () => {
  let component: TortugarioComponent;
  let fixture: ComponentFixture<TortugarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TortugarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TortugarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
