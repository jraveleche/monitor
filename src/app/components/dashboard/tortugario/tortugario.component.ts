import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataStore, Storage, Predicates, SortDirection } from 'aws-amplify';
import { UtilService } from 'src/app/services/util/util.service';
import { Catalogo, 
         Especie, 
         EspecieTortugario, 
         Hotel, 
         HotelTortugario, 
         Playa, 
         Recurso, 
         Tortugario } from 'src/models';
import { v4 as uuidv4 } from 'uuid';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/services/loader.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tortugario',
  templateUrl: './tortugario.component.html',
  styleUrls: ['./tortugario.component.scss']
})
export class TortugarioComponent implements OnInit {

  @ViewChild("fileUpload") fileUpload: ElementRef;
  @Output("onHideComponent") hide = new EventEmitter<any>();
  public fileList : any [] = [];
  files: File [] = [];
  regions : any [] = [];
  tortugario: FormGroup;
  departamentos: Catalogo [] = [];
  municipios: Catalogo [] = [];
  especies: Especie [] = [];
  hoteles: Hotel [] = [];
  playas: Playa [] = [];
  indexEspecies = 0;
  changeEspecies = false;
  indexHotel = 0;
  changeHoteles = false;
  indexPlaya = 0;
  changePlaya = false;

  constructor(private formBuilder : FormBuilder, 
              private util : UtilService, 
              private loader : LoaderService,
              private router: Router) { }

  async ngOnInit() {
    this.tortugario = this.formBuilder.group({
      nombre: ['', Validators.required],
      numeroRegistro: ['', Validators.required],
      tipoTortugario: ['', Validators.required],
      direccionConap: ['', Validators.required],
      administrarDonaciones: [false, Validators.required],
      huevosSembrados: ['', Validators.pattern('^[0-9]+')],
      neonatosLiberados: ['', Validators.pattern('^[0-9]+')],
      departamento: ['', Validators.required],
      municipio: [{value : '', disabled: true}, Validators.required],
      especies:  this.formBuilder.array([], Validators.required),
      recursos: '',
      playas: this.formBuilder.array([], Validators.required),
      hoteles: this.formBuilder.array([]),
    });
    await this.getDepartamentos();
    this.especies = await DataStore.query(Especie, Predicates.ALL, {
      sort: e => e.nombre(SortDirection.ASCENDING)
    });

    this.hoteles = await DataStore.query(Hotel, Predicates.ALL, {
      sort : e => e.nombre(SortDirection.ASCENDING)
    });

    this.playas = await DataStore.query(Playa, Predicates.ALL, {
      sort: e => e.nombre(SortDirection.ASCENDING)
    });
  }

  async getDepartamentos(){
    this.departamentos = await DataStore.query(Catalogo, c => c.catalogo('eq', 'DEPARTAMENTOS'), {
      sort: d => d.descripcion(SortDirection.ASCENDING)
    });
  }
  async getMunicipio (departamento: any){
    this.municipios = await DataStore.query(Catalogo, c => c.codigo('eq', departamento.value), {
      sort: d => d.descripcion(SortDirection.ASCENDING)
    });
    
    if(this.municipios.length > 0) {
      this.tortugario.controls.municipio.enable();
    }
  }

  setIndexEsepcie(event: any){
    let index = this.especies.indexOf(event.value);
    this.indexEspecies = index;
    this.changeEspecies = true;
  }

  setIndexHotel(event: any){
    let index = this.hoteles.indexOf(event.value);
    this.indexHotel = index;
    this.changeHoteles = true;
  }

  setIndexPlaya(event: any){
    let index = this.playas.indexOf(event.value);
    this.indexPlaya = index;
    this.changePlaya = true;
  }

  addEspecie(){
    let especie = this.especies[this.indexEspecies];
    const control = <FormArray>this.tortugario.controls.especies;
    let exist = control.controls.find(control => {
      return control.value && control.value.nombre === especie.nombre;
    });

    if(!exist && this.changeEspecies){
      control.push(new FormControl(especie));
    }
  }

  addHotel(){
    let hotel = this.hoteles[this.indexHotel];
    const control = <FormArray>this.tortugario.controls.hoteles;
    let exist = control.controls.find(control => {
      return control.value && control.value.nombre === hotel.nombre;
    });

    if(!exist && this.changeHoteles){
      control.push(new FormControl(hotel));
    }
  }

  addPlaya(){
    let playa = this.playas[this.indexPlaya];
    const control = <FormArray>this.tortugario.controls.playas;
    let exist = control.controls.find(control => {
      return control.value && control.value.nombre === playa.nombre;
    });

    if(!exist && this.changePlaya){
      control.push(new FormControl(playa));
    }
  }

  removeElement(value: any, controlsArray : FormArray){
    let index = controlsArray.controls.indexOf(value);
    controlsArray.controls.splice(index, 1);
  }

  onFileSelected(){
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        let reader = new FileReader();
        const file : File = fileUpload.files[index];
        this.files.push(file);
        reader.readAsDataURL(file);
        reader.onload = (event:any) => {
          let imag = {
            img : event.target.result,
            name: file.name,
            type: file.type
          };
          this.fileList.push(imag);
      }
      }
    };
    fileUpload.click();
  }

  onFileDeleted(index : number){
    this.fileList.splice(index, 1);
  }

  erroMessage(typeError : string, control : AbstractControl | FormArray) {
    let message = "";
    if(control.invalid && control.touched){
      switch(typeError){
        case "nombre":
          message = "Por favor ingrese un nombre para el tortugario";
          break;
        case "tipoTortugario":
          message = "Por favor seleccione el tipo de tortugario a crear";
          break;
        case "departamento":
          message = "Por favor seleccione el departamento donde se ubica el tortugario";
          break;
        case "municipio":
          message = "Por favor seleccione el municipio donde se ubica el tortugario";
          break;
        case "playas":
          message = "Por favor agregue las playas cercanas al tortugario";
          break;
        case "especies":
          message = "Por favor agregue las especies que alberga el tortugario";
          break;
        default:
          message = "";
          
      }
  
      if(message){
        this.util.openSnackBar(message, "¡Errro!");
      }
    }

  }

  confirmCreation() {

    if(this.tortugario.invalid){
      this.util.openSnackBar('Por favor verifique los campos antes de guardar la información', '¡Notificación!');
      return;
    }

    Swal.fire({
      icon: 'question',
      title: 'Guardar nuevo tortugario',
      html: `¿Desea guardar el nuevo torgurio ${this.tortugario.controls.nombre.value.toUpperCase()}?`,
      showDenyButton: true,
      focusDeny: true,
      confirmButtonColor: '#0c86cd',
      confirmButtonText: 'Crear',
      denyButtonText: 'Cancelar',
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then(result => {
      if(result.isConfirmed) {
        this.createTortugario();
      }
    });
  }

  async createTortugario(){
    if(this.tortugario.valid) {
      this.loader.show();
      let playas : Playa [] = (<FormArray>this.tortugario.controls.playas).controls.map(e => {return e.value});
      let especies : Especie [] = (<FormArray>this.tortugario.controls.especies).controls.map(e => {return e.value});
      let hoteles : Hotel [] = (<FormArray>this.tortugario.controls.hoteles).controls.map(e => {return e.value});
      let rgxNumber = /^[0-9]+/;
      let huevosSembrados = this.tortugario.controls.huevosSembrados.value;
      let huevosSembradosValue =  rgxNumber.test(huevosSembrados) ? parseFloat(huevosSembrados) : 0;
      let neonatosLiberados = this.tortugario.controls.neonatosLiberados.value;
      let neonatosLiberadosValue =  rgxNumber.test(neonatosLiberados) ? parseFloat(neonatosLiberados) : 0;

      let tortugarioModel = new Tortugario({
        nombre: this.tortugario.controls.nombre.value,
        numeroRegistro: this.tortugario.controls.numeroRegistro.value,
        tipoTortugario: this.tortugario.controls.tipoTortugario.value,
        direccionConap: this.tortugario.controls.direccionConap.value,
        fechaInicioTemporada: "",
        fechaFinTemporada: "",
        administrarDonaciones: this.tortugario.controls.administrarDonaciones.value,
        latitud: 0,
        longitud: 0,
        direccion: "",
        huevosSembrados: huevosSembradosValue,
        neonatosLiberados: neonatosLiberadosValue,
        departamento: this.tortugario.controls.departamento.value,
        municipio: this.tortugario.controls.municipio.value
      });

      try {
        let result = await DataStore.save(tortugarioModel);
        await this.saveEspecies(especies, result);
        await this.saveHoteles(hoteles, result);
        await this.saveFiles(result.id);
        Swal.fire({
          icon: 'success',
          title: 'Nuevo tortugario creado',
          html: `Se ha registrado la información correctamente`,
          focusConfirm: true,
          confirmButtonColor: '#0c86cd',
          confirmButtonText: 'Aceptar',
          allowOutsideClick: false,
          allowEscapeKey: false,
        }).then(result => {
          if(result.isConfirmed) {
            this.router.navigate(['menu']);
          }
        });
      }catch(error) {
        console.error(error);
        this.util.openSnackBar('Error al guardar el torguario, por favor inténtelo más tarde', '¡Error!');
        
      }
      this.loader.hide();

    }
  } 

  private async saveEspecies(especies: Especie [], tortugario : Tortugario){
    for(let especie of especies){
      let especieTortugario = new EspecieTortugario({
        especie: especie,
        tortugario: tortugario
      });
      await DataStore.save(especieTortugario);
    }
  }

  private async saveHoteles(hoteles: Hotel [], tortugario : Tortugario){
    for(let hotel of hoteles) {
      let hotelTortugario = new HotelTortugario({
        hotel: hotel, 
        tortugario: tortugario
      });
      await DataStore.save(hotelTortugario);
    }
  }

  private async saveFiles(tortugarioId : string){
    for(let file of this.files){
      let uuidFile = uuidv4();
      await Storage.put(`${uuidFile}.jpeg`, file, {
        contentType: 'image/jpeg.'
      });
      let recurso = new Recurso({
        tortugarioID: tortugarioId,
        tipo: 'imagen',
        archivoID: uuidFile,
        bucket: environment.bucket,
        region: environment.region,
      });
      await DataStore.save(recurso);
    }
  }

  cancel() {
    this.hide.emit(
      {
        show: true,
        section: 'home'
      });
  }

}
