import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-card-menu',
  templateUrl: './card-menu.component.html',
  styleUrls: ['./card-menu.component.scss']
})
export class CardMenuComponent implements OnInit {

  @Input("element") element: any;
  @Output("onHideComponent") hide = new EventEmitter<any>();
  section = {};

  constructor() { }

  ngOnInit(): void {
  }

  send() {
    this.element['show'] = true;
    this.hide.emit(this.element);
  }

}
