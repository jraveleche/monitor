import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchInspeccionComponent } from './search-inspeccion.component';

describe('SearchInspeccionComponent', () => {
  let component: SearchInspeccionComponent;
  let fixture: ComponentFixture<SearchInspeccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchInspeccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchInspeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
