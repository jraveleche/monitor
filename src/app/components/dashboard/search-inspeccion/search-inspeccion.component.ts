import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataStore } from 'aws-amplify';
import { Inspeccion, Tortugario } from 'src/models';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-search-inspeccion',
  templateUrl: './search-inspeccion.component.html',
  styleUrls: ['./search-inspeccion.component.scss']
})
export class SearchInspeccionComponent implements OnInit {

  
  dataSource: MatTableDataSource<any>;
  @Input("tortugarioId") tortugarioId : string;
  @Input("nombreTortugario") nombreTortugario: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['fecha', 'horaInicio', 'horaFin', 'comprobante', 'nombreTortugario', 'direccionRegional', 'estado', 'acciones'];
  title: string;
  showTable: boolean =  false;

  constructor() { }

  ngOnInit(): void {
    this.getListInspecciones();
  }


  async getListInspecciones() {
    let inspecciones = await DataStore.query(Inspeccion, i => i.torguatioID('eq', this.tortugarioId));
    this.showTable = inspecciones.length >  0;
    let result = await Promise.all(inspecciones.map(async e => {
      let tortugario = await DataStore.query(Tortugario, e.torguatioID);
      let objectResult = {...e};
      objectResult['nombreTortugario'] = tortugario.nombre.toUpperCase();
      return objectResult;
    }));
    this.dataSource = new MatTableDataSource(result);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    console.log("buscando");
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openPdf(id : string){
    window.open(`${environment.monitorApi}inspecciones/${id}/report`, '_blank');
  }

}
