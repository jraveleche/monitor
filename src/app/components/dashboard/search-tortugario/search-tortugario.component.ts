import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataStore, Predicates } from 'aws-amplify';
import { Tortugario } from 'src/models';

@Component({
  selector: 'app-search-tortugario',
  templateUrl: './search-tortugario.component.html',
  styleUrls: ['./search-tortugario.component.scss']
})
export class SearchTortugarioComponent implements OnInit {

  dataSource: MatTableDataSource<Tortugario>;

  @Input("typeReport") typeReport : string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  tortugarioId: string;
  nombreTortugario: string;
  displayedColumns: string[] = ['nombre', 'numeroRegistro', 'tipoTortugario', 'direccionConap', 'departamento', 'municipio', 'acciones'];
  title: string;
  views = {};

  constructor() { 
  }

  ngOnInit(): void {
    if(this.typeReport === 'reporteVaramiento'){
      this.title = 'Seleccione el tortugario para poder ver los varamientos registrados';
    }else if(this.typeReport === 'reporteInspeccion'){
      this.title = 'Seleccione el tortugario para poder ver las inspecciones registradas';
    }else if(this.typeReport === 'reporteDatosMensuales'){
      this.title = 'Seleccione el tortugario para poder ver la boleta de datos mensuales';
    }

    this.views['home'] = true;
    this.getListTortugarios();
  }

  async getListTortugarios() {
    let result = await DataStore.query(Tortugario, Predicates.ALL);
    this.dataSource = new MatTableDataSource(result);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    console.log("buscando");
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showReport(id: string, tortugario: Tortugario){
    this.views = {};
    this.views[id] = true;
    this.tortugarioId = tortugario.id;
    this.nombreTortugario = tortugario.nombre;
  }

}
