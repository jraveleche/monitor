import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTortugarioComponent } from './search-tortugario.component';

describe('SearchTortugarioComponent', () => {
  let component: SearchTortugarioComponent;
  let fixture: ComponentFixture<SearchTortugarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchTortugarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTortugarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
