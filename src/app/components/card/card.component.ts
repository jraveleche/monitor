import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MapDialogComponent } from '../dialogs/map-dialog/map-dialog.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {


  @Input("option") option: any;
  @Input("color") color: string;
  @Output("onShowInformationDialog") informationDailog = new EventEmitter<any>();
  boderBottom : string = "";
  constructor(private router : Router, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.boderBottom = `5px solid ${this.color ? this.color : 'blue'}`;
  }
  
  goToPage() {

    if(!this.option) {
      return;
    }

    if(this.option.openMap) {
      this.dialog.open(MapDialogComponent, {
        panelClass: 'custom-container',
        width: '800px',
        scrollStrategy: new NoopScrollStrategy(),
      });
      return;
    }

    if(this.option.goTo) {
      if(this.option.queryParams) {
        this.router.navigate([this.option.goTo], {
          queryParams: {
            ...this.option.queryParams
          }
        });
        return;
      }
      this.router.navigate([this.option.goTo]);
    }

    if(this.option.openDialog) {
      let data = {
        title: this.option.titleDialog,
        markdownSrc: this.option.markdownSrc
      }
      this.informationDailog.emit(data);
    }
  }

}
