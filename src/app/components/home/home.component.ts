import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStore, Predicates } from 'aws-amplify';
import { UtilService } from 'src/app/services/util/util.service';
import { Tortugario } from 'src/models';
import { InformationDialogComponent } from '../dialogs/information-dialog/information-dialog.component';
import { ResumeTortugarioDialogComponent } from '../dialogs/resume-tortugario-dialog/resume-tortugario-dialog.component';
const frames = require('../../services/util/frames.json');  
const tortugariosOptions = require('../../services/util/tortugarioOptions.json');
const reportsOptions = require('../../services/util/reportsOptions.json');
const donantesOptions = require('../../services/util/donantesOptions.json');
const liberacionOptions = require('../../services/util/liberacionOptions.json');


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  framesArray : any = frames;
  tortugarioOptionsArray: any = tortugariosOptions;
  donantesOptionsArray: any = donantesOptions;
  liberacionOptionsArray: any = liberacionOptions;
  reportesOptionsArray: any = reportsOptions;
  //estas son coordenadas de la capital, cambiarlas por las coordenadas de el sur 
  center = {
    lat: 14.68945469118511,
    lng: -90.51306398302323
  }
  markers : any [] = [];
  infoContent = ''
  zoom = 11;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow: MapInfoWindow;
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap;

  constructor(
    private router : Router,
    private util : UtilService,
    private activeRoute: ActivatedRoute,
    private dialog : MatDialog) { }

  ngOnInit(): void {
    this.getTortugarios();
    window.addEventListener('scroll', this.scroll, true);
    this.util.renderSection().subscribe( param => {
      const section = document.getElementById(param.section);
      section?.scrollIntoView(true);
      let scrolledY = document.body.scrollTop;
      if(scrolledY && !param.isPrincipal) {
        document.body.scroll(0, scrolledY - 135);
      }
    });
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll',this.scroll, true);
  }

  ngAfterViewInit(): void {
    this.scrollToSection();
  }

  scrollToSection() {

    this.activeRoute.params.subscribe(param => {
      if(param.section){
        const section = document.getElementById(param.section);
        section?.scrollIntoView();
      }
    })
  }

  scroll = (event : any): void  => {
    let element = document.getElementById('generalInformation');
    if(!element.classList.contains('disable-hover')) {
      element.classList.add('disable-hover');
    }
    setTimeout(function() {
      element.classList.remove('disable-hover');
    }, 2000);
  }

  showTortugariResume(idTortugario: string) {
    this.dialog.open(ResumeTortugarioDialogComponent, {
      disableClose: true,
      panelClass: 'custom-container',
      height: '480px',
      data: {
        tortugarioId: idTortugario
      }
    });
  }

  async getTortugarios() {
    this.markers = [];
    let tortugarios = await DataStore.query(Tortugario, Predicates.ALL);
    for(let tortugario of tortugarios){
      if(tortugario.latitud && tortugario.longitud) {
        this.markers.push({
          tortugarioId: tortugario.id,
          position: {
            lat: tortugario.latitud + ((Math.random() - 0.5) * 2) / 10, // eliminar esto, es solo para ejemplo
            lng: tortugario.longitud + ((Math.random() - 0.5) * 2) / 10
          },
          label: {
            color: 'red',
            text: tortugario.nombre
          },
          title: `${tortugario.nombre.toUpperCase()}`,
          options: {
            animation: { animation: google.maps.Animation.BOUNCE }
          }
        });
      }
    }
  }

  openInfo(marker: MapMarker, info: any) {
    this.infoContent = info.title;
    this.infoWindow.open(marker);
    this.showTortugariResume(info.tortugarioId);
  }

  showInformationDialog(data: any, height?: number) {
    this.dialog.open(InformationDialogComponent , {
      disableClose: true,
      panelClass: 'custom-container',
      scrollStrategy: new NoopScrollStrategy(),
      data: data
    })
  }

  readMore() {
    let data = {
      title: '¿Qué es moniTOR?',
      markdownSrc: 'assets/markdown/que-es-monitor.md'
    }
    this.showInformationDialog(data);
  }
}
