import { DOCUMENT } from '@angular/common';
import { ChangeDetectionStrategy, Component, HostListener, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { UtilService } from 'src/app/services/util/util.service';
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {
  
  showNavbar : boolean = true;
  iframes : any [];

 
  constructor
  (
    private util : UtilService, 
    private utilService: UtilService,
    readonly renderer2: Renderer2, 
    @Inject(DOCUMENT) private _document: Document
  ){
  }


  ngOnInit(): void {
    this.util.hideComponenet().subscribe(response => {
      this.showNavbar = !response;
    });
    $('.navbar-nav>li>a').on('click', function(){
      $('.navbar-collapse').collapse('hide');
    });
    window.addEventListener('scroll', this.scroll, true);
  }

  scroll = (event : any): void  => {
    let scroll  = document.body.scrollTop;
    let element = document.getElementById('navHeader');
    if(scroll > 1){
      element.classList.add('active-header');
    }else {
      element.classList.remove('active-header');
    }
  }
  

  ngOnDestroy(): void {
      window.removeEventListener('scroll',this.scroll, true);
  }

  readMore() {
    let data = {
      title: '¿Qué es moniTOR?',
      markdownSrc: 'assets/markdown/que-es-monitor.md'
    }
   
    this.utilService.showInformationDialog(data, 600);
  }

  comoApoyar() {
    let data = {
      title: 'Cómo apoyar',
      markdownSrc: 'assets/markdown/como-apoyar.md'
    }
    this.utilService.showInformationDialog(data, 600);
  }

}
