import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDonacionComponent } from './search-donacion.component';

describe('SearchDonacionComponent', () => {
  let component: SearchDonacionComponent;
  let fixture: ComponentFixture<SearchDonacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchDonacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDonacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
