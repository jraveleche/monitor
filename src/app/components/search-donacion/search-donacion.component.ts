import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Auth, DataStore, Predicates } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { UserUtilService } from 'src/app/services/user-util.service';
import { UtilService } from 'src/app/services/util/util.service';
import { DonacionEconomica, DonacionHuevos, Tortugario, Usuario } from 'src/models';
import { VerificationDialogComponent } from '../dialogs/verification-dialog/verification-dialog.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search-donacion',
  templateUrl: './search-donacion.component.html',
  styleUrls: ['./search-donacion.component.scss']
})
export class SearchDonacionComponent implements OnInit {

  form: FormGroup;
  showDonaciones : boolean = false;
  dataSourceHuevos: MatTableDataSource<any>;
  dataSourceEconomica: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumnsEggs: string[] = ['nombre', 'fecha', 'cantidadHuevos', 'nombreTortugario', 'status', 'acciones'];
  displayedColumnsEconomic: string[] = ['nombre', 'fecha', 'monto', 'montoUtilizado','nombreTortugario', 'status', 'acciones'];
  hide: boolean = true;

  constructor(
    private formBuilder : FormBuilder,
    private userService: UserUtilService,
    private dialog : MatDialog,
    private util: UtilService, 
    private loader: LoaderService,) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async searchAllDonaciones(){
    this.loader.show();
    await DataStore.start();
    if(this.form.invalid){
      this.util.openSnackBar('Por favor complete los campos del formulario.', '¡Notificación!');
      return;
    }

    await Auth.signOut();
    let username = this.form.controls.username.value;
    let usuarios = await DataStore.query(Usuario, Predicates.ALL);
    let existDonante = usuarios.find(usr => usr.username.toLowerCase() === username.toLowerCase());
    if(!existDonante){
      this.util.openSnackBar('Por favor ingrese un usuario valido', '¡Notificación!');
      this.loader.hide();
      return;
    }

    let singInResult = await this.singin(existDonante);
    if(!singInResult) {
      return;
    }

    let donante = existDonante;
    let donacionEconomica = await DataStore.query(DonacionEconomica, e => e.donanteID('eq', donante.id));
    let donacionHuevos = await DataStore.query(DonacionHuevos, e => e.donanteID('eq', donante.id));
    
    this.createDataSourceEggs(donacionHuevos, donante);
    this.createDataSourceEconomic(donacionEconomica, donante);
    this.showDonaciones = donacionEconomica.length > 0 || donacionHuevos.length > 0;
    if(donacionEconomica.length === 0 && donacionHuevos.length === 0){
      await Auth.signOut();
      Swal.fire({
        icon: 'error',
        title: 'No hay donaciones',
        html: `Aún no realizado ningúna donación.`,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
      });
    }
    this.loader.hide();
  }

  private async  createDataSourceEggs(donaciones : DonacionHuevos[], usuario: Usuario) {
    let objectResultEggs = await Promise.all(donaciones.map(async dh =>{
      let tortugario = await DataStore.query(Tortugario, dh.tortugarioID);
      return {
        id: dh.id,
        nombre: this.getDonanteName(usuario),
        fecha: dh.fecha,
        cantidadHuevos: dh.cantidadHuevos,
        nombreTortugario: tortugario.nombre ? tortugario.nombre.toUpperCase() : '',
        status: this.getStatus(dh),
      }
    }));

    this.dataSourceHuevos = new MatTableDataSource(objectResultEggs);
    this.dataSourceHuevos.paginator = this.paginator;
    this.dataSourceHuevos.sort = this.sort;
  }

  private async createDataSourceEconomic(donaciones: DonacionEconomica[], usuario : Usuario){
    let objectResultEconomic = await Promise.all(donaciones.map(async de =>{
      let tortugario = await DataStore.query(Tortugario, de.tortugarioID);
      return {
        id: de.id,
        nombre: this.getDonanteName(usuario),
        fecha: de.fecha,
        monto: de.monto,
        montoUtilizado: de.montoUtilizado ? de.montoUtilizado : 0,
        nombreTortugario: tortugario.nombre ? tortugario.nombre.toUpperCase() : '',
        status: this.getStatus(de),
      }
    }));

    this.dataSourceEconomica = new MatTableDataSource(objectResultEconomic);
    this.dataSourceEconomica.paginator = this.paginator;
    this.dataSourceEconomica.sort = this.sort;
  }

  private getStatus(donacion: DonacionEconomica | DonacionHuevos) {
    switch(donacion.status) {
      case 'available':
        return 'Disponible';
      case 'empty':
        return 'Vacío';
    }
  }

  private getDonanteName(usuario : Usuario) {
    if(!usuario){
      return "";
    }

    let names = `${usuario.primerNombre ? usuario.primerNombre : ''} ${usuario.segundoNombre ? usuario.segundoNombre : ''}`;
    let lastnames = `${usuario.primerApellido ? usuario.primerApellido : ''} ${usuario.segundoApellido ? usuario.segundoApellido : ''}`;
    let completName = `${names} ${lastnames}`;
    completName = completName.replace(/\s+/, ' ');
    completName = completName.toUpperCase();
    return completName.trim();
  }

  clearBusquedaTortugario() {
    this.showDonaciones = false;
    this.dataSourceEconomica = new MatTableDataSource([]);
    this.dataSourceHuevos = new MatTableDataSource([]);
    this.form.reset();
  }

  private async singin(user : Usuario){
    try{
      let roles = /\bADMINISTRADOR\b|\bCONAP\b|\bDONADOR\b|\bROOT\b$/
      if(!roles.test(user.rol)) {
        this.loader.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          html: `Lo sentimos no tiene el rol necesario para poder utilizar la aplicación`,
          confirmButtonColor: '#0c86cd',
          confirmButtonText: 'Aceptar',
        });
        return;
      }
      
      if(!user.confirmado){
        this.loader.hide();
        this.dialog.open(VerificationDialogComponent, {
          data: {
            isVerificationScreen: true,
            user: user
          }, 
          disableClose: true,
          panelClass: 'custom-container'
        });
        return;
      }
      let resutlSignIn = await this.userService.signIn(user.id, this.form.controls.password.value);
      if(!resutlSignIn) {
        this.loader.hide();
        Swal.fire({
          icon: 'error',
          title: 'Error de autenticación',
          html: `Usuario o contraseña incorrectos, por favor intente nuevamente`,
          confirmButtonColor: '#0c86cd',
          confirmButtonText: 'Aceptar',
        });
        return;
      }

      sessionStorage.setItem('actual-user', JSON.stringify(user));
      this.loader.hide();
      return true;
    }catch(error){
      console.error("error *****", error)
      this.loader.hide();
    }
    return false;
  }
}
