import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/services/util/util.service';
const webFiles = require('../../services/util/webFiles.json');
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  showFooter : boolean = true;
  webFilesArray : any [] = webFiles;

  constructor(public util: UtilService) { }

  ngOnInit(): void {
    this.util.hideComponenet().subscribe(response => {
      this.showFooter = !response;
    });
  }


  readMore() {
    let data = {
      title: 'Créditos',
      markdownSrc: 'assets/markdown/creditos.md'
    }
    this.util.showInformationDialog(data);
  }

  comoApoyar() {
    let data = {
      title: 'Cómo apoyar',
      markdownSrc: 'assets/markdown/como-apoyar.md'
    }
    this.util.showInformationDialog(data);
  }

  queEsMonitor() {
    let data = {
      title: '¿Qué es moniTOR?',
      markdownSrc: 'assets/markdown/que-es-monitor.md'
    }
    this.util.showInformationDialog(data);
  }

}
