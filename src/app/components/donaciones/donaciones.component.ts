import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Auth, DataStore, Predicates } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { CompraDonacionJoin, DonacionEconomica, DonacionHuevos, Tortugario, Playa, Sector } from 'src/models';

@Component({
  selector: 'app-donaciones',
  templateUrl: './donaciones.component.html',
  styleUrls: ['./donaciones.component.scss'],
  providers:[{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class DonacionesComponent implements OnInit {
  donacion : DonacionEconomica | DonacionHuevos;

  dataSource: MatTableDataSource<any>;
  dataSourceEggs: MatTableDataSource<any>;
  donacionEconomica: DonacionEconomica;
  donacionHuevos: DonacionHuevos
  montoUtilizado = 0;
  typeDonacion = '';
  displayedColumns: string[] = ['especieProbable', 'cantidadHuevos', 'fecha', 'monto', 'playa', 'tortugario'];
  displayedColumnsEggs: string []  = ['especieProbable', 'cantidadHuevos', 'fecha', 'tortugario', 'playa', 'sector'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  loading = false;

  constructor(private route: ActivatedRoute, private loader: LoaderService, private router : Router) {}

  ngOnInit(): void {
    this.initSeguimiento();
  }

  async initSeguimiento() {
    this.loader.show();
    await DataStore.start();
    this.route.queryParamMap.subscribe( async (params) => {
      this.typeDonacion = params.get('type');
      if(this.typeDonacion === 'huevos'){
        await this.getDonacionesHuevos(params.get('id'));
      }else if(this.typeDonacion === 'economica'){
        await this.getDonacionesEconomicas(params.get('id'))
      }
      this.loader.hide();
    });
  }

  async getDonacionesEconomicas(id: string) {
    this.donacionEconomica = await DataStore.query(DonacionEconomica, id);
    let montoDonacion = this.donacionEconomica ? this.donacionEconomica.monto : 1;
    this.montoUtilizado = 0;
    let compras = (await DataStore.query(CompraDonacionJoin, Predicates.ALL)).filter(cd => cd.donacionEconomica.id === id);

    let dataSource = await Promise.all(compras.map( async c => {
      let compra = c.CompraHuevos;
      let tortugario = await DataStore.query(Tortugario, compra.tortugarioID);
      let playa = await DataStore.query(Playa, compra.playaID);
      this.montoUtilizado += c.monto;

      return  {
        'especieProbable': compra.especieProbable,
        'cantidadHuevos': compra.cantidadHuevos,
        'fecha': compra.createdAt,
        'monto' : compra.monto,
        'playa': playa ? playa.nombre : '',
        'tortugario': tortugario ? tortugario.nombre : ''
      }
    }));
    this.dataSource = new MatTableDataSource(dataSource);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    let porcentaje = (this.montoUtilizado*100) / montoDonacion; 
    this.setProgress(`${porcentaje > 100 ? 100 : porcentaje}`);
  }

  async getDonacionesHuevos(id : string) {
    this.donacionHuevos = await DataStore.query(DonacionHuevos, id);
    let donacionesList = (await DataStore.query(DonacionHuevos, Predicates.ALL)).filter(dh => dh.donanteID === this.donacionHuevos.donanteID);    
    let dataSource = await Promise.all(donacionesList.map( async c => {
      let tortugario = await DataStore.query(Tortugario, this.donacionHuevos.tortugarioID);
      let playa = await DataStore.query(Playa, this.donacionHuevos.playaID);
      let sector = await DataStore.query(Sector, this.donacionHuevos.sectorID);

      return  {
        'especieProbable': c.especieProbable,
        'cantidadHuevos': c.cantidadHuevos,
        'fecha': c.createdAt,
        'tortugario': tortugario ? tortugario.nombre : '',
        'playa': playa ? playa.nombre : '',
        'sector': sector ? sector.nombre : '', 
      }
    }));
    this.dataSourceEggs = new MatTableDataSource(dataSource);
    this.dataSourceEggs.paginator = this.paginator;
    this.dataSourceEggs.sort = this.sort;
  }

  setProgress(progress: string) {
    let value = `${progress}%`;
    let div = document.getElementById('inputDiv');
    div.style.setProperty('--progress', value);
    div.innerHTML = value;
    div.setAttribute('aria-valuenow', value)
  }

  async backPage() {
    await Auth.signOut();
    this.router.navigate(['buscar-donaciones']);
  }
}
