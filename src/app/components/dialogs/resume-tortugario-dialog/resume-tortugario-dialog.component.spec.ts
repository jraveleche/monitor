import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeTortugarioDialogComponent } from './resume-tortugario-dialog.component';

describe('ResumeTortugarioDialogComponent', () => {
  let component: ResumeTortugarioDialogComponent;
  let fixture: ComponentFixture<ResumeTortugarioDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumeTortugarioDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeTortugarioDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
