import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataStore, Predicates } from 'aws-amplify';
import * as moment from 'moment';
import { LoaderService } from 'src/app/services/loader.service';
import { Hotel, Tortugario } from 'src/models';
import { ContactoDialogComponent } from '../contacto-dialog/contacto-dialog.component';

@Component({
  selector: 'app-resume-tortugario-dialog',
  templateUrl: './resume-tortugario-dialog.component.html',
  styleUrls: ['./resume-tortugario-dialog.component.scss']
})
export class ResumeTortugarioDialogComponent implements OnInit {

  tortugario: Tortugario;
  fechaInicio: string;
  fechaFin: string;

  constructor(
    private dialog: MatDialogRef<ResumeTortugarioDialogComponent>,
    private dialog_ : MatDialog,
    @Inject(MAT_DIALOG_DATA) public data : any,
    private loaderService: LoaderService) { }

  ngOnInit(): void {
    console.log(this.data);
    this.getTortugarioData();
  }

  async getTortugarioData() {
    this.loaderService.show();
    let tortugarioId = this.data.tortugarioId;
    this.tortugario = await DataStore.query(Tortugario, tortugarioId);
    this.setStartAndEndDate(this.tortugario.fechaInicioTemporada, this.tortugario.fechaFinTemporada);
    this.loaderService.hide();
  }

  setStartAndEndDate( startDate: string, endDate: string) {
    try {
      if(startDate) {
        this.fechaInicio = moment(new Date(startDate)).format('DD/MM/yyyy');
      }

      if(endDate) {
        this.fechaFin = moment(new Date(endDate)).format('DD/MM/yyyy');
      }
    }catch(error) {
      console.error("Error al convertir fechas ", error);
    }
  } 

  close() {
    this.dialog.close();
  }

  contactDialog() {
    this.dialog_.open(ContactoDialogComponent, {
      disableClose: true,
      panelClass: 'custom-container',
      scrollStrategy: new NoopScrollStrategy(),
      data: {
        idTortugario: this.data.tortugarioId
      }
    });
  }

}
