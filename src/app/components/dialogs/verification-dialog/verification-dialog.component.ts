import { trigger, transition, style, animate } from '@angular/animations';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Auth, DataStore } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { UserUtilService } from 'src/app/services/user-util.service';
import { UtilService } from 'src/app/services/util/util.service';
import { Usuario } from 'src/models';

@Component({
  selector: 'app-verification-dialog',
  templateUrl: './verification-dialog.component.html',
  styleUrls: ['./verification-dialog.component.scss'],
  animations: [
  trigger(
    'enterAnimation', [
      transition(':enter', [
        style({transform: 'translateX(100%)', opacity: 0}),
        animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateX(0)', opacity: 1}),
        animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
      ])
    ]
  )
]
})
export class VerificationDialogComponent implements OnInit {

  codeForm: FormGroup;
  resetPassWordForm: FormGroup;
  resetPasswordScreen: boolean = false;
  hide: boolean = true;

  constructor(
    private dialog : MatDialogRef<VerificationDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private formBuilder : FormBuilder,
    private loaderService: LoaderService,
    private util: UtilService,
    private userUtil: UserUtilService,
    private router: Router) { }

  ngOnInit(): void {
    this.codeForm = this.formBuilder.group({
      d1 : ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/g), Validators.maxLength(1)])],
      d2 : ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/g), Validators.maxLength(1)])],
      d3 : ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/g), Validators.maxLength(1)])],
      d4 : ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/g), Validators.maxLength(1)])],
      d5 : ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/g), Validators.maxLength(1)])],
      d6 : ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/g), Validators.maxLength(1)])],
    });

    this.resetPassWordForm = this.formBuilder.group({
      password: ['', Validators.required],
    });

  }
  
  async resendCode() {
    try {
      this.loaderService.show();
      let user : Usuario = this.data.user;
      await Auth.forgotPassword(user.username);
      this.loaderService.hide();
      this.util.openSnackBar('Se ha enviado un nuevo código por favor revisa su correo', '¡Notificación!');
    }catch(error){
      console.error("Hubo un error al enviar el código ", error);
      this.loaderService.hide();
      this.util.openSnackBar('Hubo un error al enviar el código de verificación', '¡Notificación!');
    }

  }

  async verifyCode() {
    if(this.codeForm.invalid) {
      this.util.openSnackBar('Por favor ingrese un código válido.', '¡Notificación!');
      return;
    }
    this.resetPasswordScreen = true;
  }

  async changePassword() {
    if(this.resetPassWordForm.invalid) {
      this.util.openSnackBar('Por favor ingrese su nueva contraseña', '¡Notificación!');
      return;
    }

    let verificationCode = '';
    Object.keys(this.codeForm.controls).forEach(e => {
      verificationCode = `${verificationCode}${this.codeForm.get(e).value}`;
    });

    this.loaderService.show();
    try {
      let user : Usuario = this.data.user;
      await Auth.forgotPasswordSubmit(user.id, verificationCode, this.resetPassWordForm.controls.password.value);
      this.dialog.close(true);
      this.loaderService.hide();
    }catch(error){
      console.error('Hubo un error al verificar el usuario', error);
      this.loaderService.hide();
      this.util.openSnackBar('Hubo un error al verificar al usuario','¡Notificación!');
    }
  }

  async verifyNewUser() {

    if(this.codeForm.invalid) {
      this.util.openSnackBar('Por favor ingrese un código válido.', '¡Notificación!');
      return;
    }

    let user : Usuario = this.data.user;
    let verificationCode = '';
    Object.keys(this.codeForm.controls).forEach(e => {
      verificationCode = `${verificationCode}${this.codeForm.get(e).value}`;
    });
    this.loaderService.show();
    let verificated = await this.userUtil.confimUser(user.username, verificationCode);
    if(!verificated){
      this.loaderService.hide();
      this.util.openSnackBar('Hubo un error al verificar al usuario, por favor intenta nuevamente', '¡Notificación!');
      return;
    }

    let original = await DataStore.query(Usuario, user.id);
    let result = await DataStore.save(Usuario.copyOf(original, updated => {
      updated.confirmado = true;
    }));
    let resultSignIn = await this.userUtil.signIn(user.id, this.data.password);
    if(!resultSignIn) {
      this.loaderService.hide();
      this.util.openSnackBar('Hubo un error al iniciar sesión, por favor intenta nuevamente', '¡Notificación!');
      return;
    }
    this.loaderService.hide();
    this.dialog.close();
    sessionStorage.setItem('actual-user', JSON.stringify(result));
    this.router.navigate(['menu']);
  }

}
