import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataStore } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { Tortugario, Usuario } from 'src/models';
import { VerificationDialogComponent } from '../verification-dialog/verification-dialog.component';
import Swal from 'sweetalert2'
import { AppService } from 'src/app/services/app.service';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-contacto-dialog',
  templateUrl: './contacto-dialog.component.html',
  styleUrls: ['./contacto-dialog.component.scss']
})
export class ContactoDialogComponent implements OnInit {


  contactForm: FormGroup;
  hide = true;
  DEFUALT_EMAILS = [
    "abegem@hotmail.com",
    "blopezw@yahoo.com"
  ];

  constructor( 
    private dialog : MatDialogRef<VerificationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any,
    private lodaerService: LoaderService,
    private appService : AppService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      phone: ['', Validators.required]
    });
  }

  close() {
    this.dialog.close();
  }

  async sendMail() {
    this.lodaerService.show();
    let idTortugario = this.data.idTortugario;
    let tortugario = await DataStore.query(Tortugario, idTortugario);
    let users = await DataStore.query(Usuario, u => u.tortugarioId('eq', tortugario.id).or(
      e => e.rol('eq', 'ADMINISTRADOR').or(
        e => e.rol('eq', 'PROMOTOR')
      )
    ));

    if(!users || users.length === 0) {
      Swal.fire({
        icon: 'info',
        title: '¡Notificación!',
        html: `No hay administradores o promotores asociados a este tortugario.`,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
      });
      this.lodaerService.hide();
      return;
    }

    let emails = users.map(e =>{
      return e.email
    });

    let subject = `Solicitud de Información ${moment(new Date()).format('DD-MM-yyyy')}`;
    let emailRequest = {
      to: environment.production ? emails : this.DEFUALT_EMAILS,
      template: 'contactUser',
      subject,
      params: {
        ...this.contactForm.getRawValue()
      }
    }
    let result = await this.appService.sendEmail(emailRequest);
    this.lodaerService.hide();
    if(result && result.status === 200) {
      Swal.fire({
        icon: 'info',
        title: '¡Notificación!',
        html: `Tu información se ha enviado correctamente`,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
      });
    }else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        html: `No se ha podido enviar su información por favor intenta más tarde`,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
      });
    }
    this.dialog.close();
  }

}
