import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MapInfoWindow, GoogleMap, MapMarker } from '@angular/google-maps';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DataStore, Predicates } from 'aws-amplify';
import { Tortugario } from 'src/models';
import { ResumeTortugarioDialogComponent } from '../resume-tortugario-dialog/resume-tortugario-dialog.component';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.scss']
})
export class MapDialogComponent implements OnInit {
  center = {
    lat: 14.68945469118511,
    lng: -90.51306398302323
  }
  markers : any [] = [];
  infoContent = ''
  zoom = 11;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow: MapInfoWindow;
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
  constructor(private dialog : MatDialogRef<MapDialogComponent>, private dialog_ : MatDialog) { }

  ngOnInit(): void {
    this.getTortugarios();
  }


  async getTortugarios() {
    this.markers = [];
    let tortugarios = await DataStore.query(Tortugario, Predicates.ALL);
    for(let tortugario of tortugarios){
      if(tortugario.latitud && tortugario.longitud) {
        this.markers.push({
          tortugarioId: tortugario.id,
          position: {
            lat: tortugario.latitud + ((Math.random() - 0.5) * 2) / 10, // eliminar esto, es solo para ejemplo
            lng: tortugario.longitud + ((Math.random() - 0.5) * 2) / 10
          },
          label: {
            color: 'red',
            text: tortugario.nombre
          },
          title: `${tortugario.nombre.toUpperCase()}`,
          options: {
            animation: { animation: google.maps.Animation.BOUNCE }
          }
        });
      }
    }
  }


  showTortugariResume(idTortugario: string) {
    this.dialog_.open(ResumeTortugarioDialogComponent, {
      disableClose: true,
      panelClass: 'custom-container',
      height: '480px',
      scrollStrategy: new NoopScrollStrategy(),
      data: {
        tortugarioId: idTortugario
      }
    });
  }

  openInfo(marker: MapMarker, info: any) {
    this.infoContent = info.title;
    this.infoWindow.open(marker);
    this.showTortugariResume(info.tortugarioId);
  }

  close() {
    this.dialog.close();
  }

}
