import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Auth, DataStore, Predicates } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { UserUtilService } from 'src/app/services/user-util.service';
import { UtilService } from 'src/app/services/util/util.service';
import { Usuario } from 'src/models';
import { VerificationDialogComponent } from '../dialogs/verification-dialog/verification-dialog.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username : string = "";
  password: string = "";
  hide = true;
  loginForm: FormGroup;
  recoveryForm: FormGroup;
  recoveryPasswordScreen: boolean = false;
  isFromExternalPage: boolean = false;
  externalPage : string = '';


  constructor(
    private router : Router, 
    private route: ActivatedRoute,
    private util: UtilService, 
    private userUtil : UserUtilService,
    private loaderService: LoaderService,
    private dialog : MatDialog,
    private formBuilder: FormBuilder) { 
  }

  ngOnInit(): void {


    this.route.queryParamMap.subscribe( async (params) => {
      this.recoveryPasswordScreen = params.get("recoveryPassword") === "true";
      this.externalPage = params.get('from');
      if(this.externalPage) {
        this.isFromExternalPage = true;
      }
    });

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.recoveryForm = this.formBuilder.group({
      username: ['', Validators.required]
    });
  }

  async singin(){

    if(this.loginForm.invalid) {
      this.util.openSnackBar('Por favor llene los campos para poder iniciar sesión' ,'¡Notificación!');
      return;
    }

    this.loaderService.show();
    let users = await DataStore.query(Usuario, Predicates.ALL);
    let user = users.find(usr => usr.username.toLowerCase() === this.loginForm.controls.username.value.toLowerCase());
    
    if(!user) {
      this.loaderService.hide();
      this.util.openSnackBar('Usuario o contraseña incorrectos' ,'¡Notificación!');
      return;
    }

    try{

      let user = users[0];
      let roles = /\bADMINISTRADOR\b|\bCONAP\b|\bROOT\b$/
      if(!roles.test(user.rol)) {
        this.loaderService.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          html: `Lo sentimos no tiene el rol necesario para poder utilizar la aplicación`,
          confirmButtonColor: '#0c86cd',
          confirmButtonText: 'Aceptar',
        });
        return;
      }
      
      if(!user.confirmado){
        this.loaderService.hide();
        this.dialog.open(VerificationDialogComponent, {
          data: {
            isVerificationScreen: true,
            user: user,
            password: this.loginForm.controls.password.value
          }, 
          disableClose: true,
          panelClass: 'custom-container'
        });
        return;
      }
      let resutlSignIn = await this.userUtil.signIn(user.id, this.loginForm.controls.password.value);
      if(!resutlSignIn) {
        this.loaderService.hide();
        Swal.fire({
          icon: 'error',
          title: 'Error de autenticación',
          html: `Usuario o contraseña incorrectos, por favor intente nuevamente`,
          confirmButtonColor: '#0c86cd',
          confirmButtonText: 'Aceptar',
        });
        return;
      }

      sessionStorage.setItem('actual-user', JSON.stringify(user));
      this.loaderService.hide();
      this.router.navigate(['menu']);
    }catch(error){
      console.error("error *****", error)
      this.loaderService.hide();
    }
  }

  async sendVerificationCode() {
    if(this.recoveryForm.invalid) {
      this.util.openSnackBar('Por favor ingrese su usuario para poder reiniciar la contraseña' ,'¡Notificación!');
      return;
    }
    this.loaderService.show();
    let users = await DataStore.query(Usuario, Predicates.ALL);
    let user = users.find(usr => usr.username.toLowerCase() === this.recoveryForm.controls.username.value.toLowerCase());

    if(!user) {
      this.loaderService.hide();
      this.util.openSnackBar('El usuario ingresado no se encuentra registrado, por favor comuníquese con un supervisor' ,'¡Notificación!');
      return;
    }

    if(user.rol.toLowerCase() === 'parlamero'){   
      this.loaderService.hide();
      Swal.fire({
        icon: 'info',
        title: 'Recordatorio...',
        html: `Recuerda que su contraseña es su DPI`,
        confirmButtonColor: '#0c86cd',
        confirmButtonText: 'Aceptar',
      });
      return;
    }

    try {
      await Auth.forgotPassword(user.id);
      this.loaderService.hide();
      this.dialog.open(VerificationDialogComponent, {
        data: {
          isVerificationScreen: false,
          user: user
        }, 
        disableClose: true,
      }).afterClosed().subscribe((result : boolean) => {
        if(result)  {
          Swal.fire({
            icon: 'success',
            title: 'Notificación',
            html: `La contrasñea ha sido actualizada`,
            confirmButtonColor: '#0c86cd',
            confirmButtonText: 'Aceptar',
          }).then(result => {
            if(result.isConfirmed) {
              this.recoveryPasswordScreen = false
              if(this.isFromExternalPage)  {
                this.router.navigate([this.externalPage]);
              }
            }
          });
        }
      });
    }catch(error) {
      console.error('Hubo un error al enviar el token', error);
      this.loaderService.hide();
      this.util.openSnackBar('Hubo un error al enviar el token', '¡Notificación!');
    }

  }

}
