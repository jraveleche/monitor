import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataStore, Predicates } from 'aws-amplify';
import { LoaderService } from 'src/app/services/loader.service';
import { Tortugario } from 'src/models';
import { ContactoDialogComponent } from '../dialogs/contacto-dialog/contacto-dialog.component';
import { ResumeTortugarioDialogComponent } from '../dialogs/resume-tortugario-dialog/resume-tortugario-dialog.component';

@Component({
  selector: 'app-listado-tortugario',
  templateUrl: './listado-tortugario.component.html',
  styleUrls: ['./listado-tortugario.component.scss']
})
export class ListadoTortugarioComponent implements OnInit {

  dataSource: MatTableDataSource<Tortugario>;
  displayedColumns: string[] = ['no', 'nombreTortugario', 'municipio', 'departamento', 'masInformacion'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private loaderService: LoaderService, private dialog : MatDialog) { }

  ngOnInit(): void {
    this.getTortugarios();
  }

  async getTortugarios() {
    this.loaderService.show();
    let tortugarios = await  DataStore.query(Tortugario, Predicates.ALL);
    this.dataSource = new MatTableDataSource(tortugarios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loaderService.hide();
  }


  showTortugariResume(idTortugario: string) {
    this.dialog.open(ResumeTortugarioDialogComponent, {
      disableClose: true,
      panelClass: 'custom-container',
      height: '480px',
      data: {
        tortugarioId: idTortugario
      }
    });
  }

  contactDialog(id: any) {
    this.dialog.open(ContactoDialogComponent, {
      disableClose: true,
      panelClass: 'custom-container',
      height: '670px',
      data: {
        idTortugario: id
      }
    });
  }

}
