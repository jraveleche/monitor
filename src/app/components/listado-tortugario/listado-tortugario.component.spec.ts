import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoTortugarioComponent } from './listado-tortugario.component';

describe('ListadoTortugarioComponent', () => {
  let component: ListadoTortugarioComponent;
  let fixture: ComponentFixture<ListadoTortugarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoTortugarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoTortugarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
