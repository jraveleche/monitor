import { Pipe, PipeTransform } from '@angular/core';
import { orderBy, sortBy } from 'lodash';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(value: any[], order = '', column: string = ''): any[] {
    if (!value) { 
      return value; 
    } 

    if (!column || column === '') { 
      return sortBy(value); 
    } 
    
    if (value.length <= 1) { 
      return value; 
    }
    
    if(order === 'desc') {
      return orderBy(value, [column], ['desc']);
    }
    return orderBy(value, [column], ['asc']);
  }

}
