import { BrowserModule } from '@angular/platform-browser';
import { NgModule, SecurityContext } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter'; 
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { LoaderComponent } from './components/loader/loader.component';
import { BlockUIModule } from 'ng-block-ui';
import { BlockUIHttpModule } from 'ng-block-ui/http';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from './services/interceptor/loader.interceptor';
import { MarkdownModule } from 'ngx-markdown';
import { LoginComponent } from './components/login/login.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/dashboard/menu/menu.component';
import { CardMenuComponent } from './components/dashboard/card-menu/card-menu.component';
import { TortugarioComponent } from './components/dashboard/tortugario/tortugario.component';
import { MatSelectModule } from '@angular/material/select';
import { UtilService } from './services/util/util.service';
import { AmplifyUIAngularModule } from '@aws-amplify/ui-angular';
import { SearchTortugarioComponent } from './components/dashboard/search-tortugario/search-tortugario.component';
import { MatSortModule } from '@angular/material/sort';
import { SearchVaramientosComponent } from './components/dashboard/search-varamientos/search-varamientos.component';
import { SearchInspeccionComponent } from './components/dashboard/search-inspeccion/search-inspeccion.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DonacionesComponent } from './components/donaciones/donaciones.component';
import { SearchDonacionComponent } from './components/search-donacion/search-donacion.component';
import { SafePipe } from './pipes/safe.pipe';
import { IframeComponent } from './components/iframe/iframe.component';
import { CookieService } from 'ngx-cookie-service';
import { UserUtilService } from './services/user-util.service';
import { AuthGuard } from './classes/auth-guard';
import { VerificationDialogComponent } from './components/dialogs/verification-dialog/verification-dialog.component';
import { CreateUserComponent } from './components/dashboard/create-user/create-user.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { ResumeTortugarioDialogComponent } from './components/dialogs/resume-tortugario-dialog/resume-tortugario-dialog.component';
import { CardComponent } from './components/card/card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InformationDialogComponent } from './components/dialogs/information-dialog/information-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ListadoTortugarioComponent } from './components/listado-tortugario/listado-tortugario.component';
import { ListadoAdministradoresComponent } from './components/listado-administradores/listado-administradores.component';
import { ListadoVaramientoComponent } from './components/reports/listado-varamiento/listado-varamiento.component';
import { MatTableExporterModule } from 'mat-table-exporter';
import { ContactoDialogComponent } from './components/dialogs/contacto-dialog/contacto-dialog.component';
import { SortByPipe } from './pipes/sort-by.pipe';
import { MapDialogComponent } from './components/dialogs/map-dialog/map-dialog.component';
import { CalendarModule, DateAdapter as DateAdapter_} from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarioLiberacionComponent } from './components/calendario-liberacion/calendario-liberacion.component';
import { CommonModule } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LoaderComponent,
    LoginComponent,
    FooterComponent,
    MenuComponent,
    CardMenuComponent,
    TortugarioComponent,
    SearchTortugarioComponent,
    SearchVaramientosComponent,
    SearchInspeccionComponent,
    DonacionesComponent,
    SearchDonacionComponent,
    SafePipe,
    IframeComponent,
    VerificationDialogComponent,
    CreateUserComponent,
    ResumeTortugarioDialogComponent,
    CardComponent,
    InformationDialogComponent,
    ListadoTortugarioComponent,
    ListadoAdministradoresComponent,
    ListadoVaramientoComponent,
    ContactoDialogComponent,
    SortByPipe,
    MapDialogComponent,
    CalendarioLiberacionComponent,
  ],
  imports: [
    BrowserModule,
    GoogleMapsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRadioModule,
    MatSelectModule,
    MatIconModule,
    MatTableModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatSortModule,
    MatStepperModule,
    FormsModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MarkdownModule.forRoot({
      sanitize : SecurityContext.NONE
    }),
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    BlockUIHttpModule.forRoot(),
    BlockUIModule.forRoot({
      template: LoaderComponent
    }),
    AmplifyUIAngularModule,
    FlexLayoutModule,
    MatDialogModule,
    MatTableExporterModule,
    CalendarModule.forRoot({
      provide: DateAdapter_,
      useFactory: adapterFactory,
    }),
    FlatpickrModule.forRoot(),
    NgbModalModule,
    CommonModule,
  ],
  providers: [
    UtilService,
    CookieService,
    UserUtilService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },

    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide : MAT_DATE_LOCALE, useValue: 'es-LA'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
