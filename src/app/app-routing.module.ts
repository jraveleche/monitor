import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './classes/auth-guard';
import { CalendarioLiberacionComponent } from './components/calendario-liberacion/calendario-liberacion.component';
import { MenuComponent } from './components/dashboard/menu/menu.component';
import { DonacionesComponent } from './components/donaciones/donaciones.component';
import { HomeComponent } from './components/home/home.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { ListadoAdministradoresComponent } from './components/listado-administradores/listado-administradores.component';
import { ListadoTortugarioComponent } from './components/listado-tortugario/listado-tortugario.component';
import { LoginComponent } from './components/login/login.component';
import { ListadoVaramientoComponent } from './components/reports/listado-varamiento/listado-varamiento.component';
import { SearchDonacionComponent } from './components/search-donacion/search-donacion.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'menu',
    component: MenuComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'donaciones',
    component: DonacionesComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'listado',
    children: [
      {
        path: 'tortugarios',
        component: ListadoTortugarioComponent
      },
      {
        path: 'administradores',
        component: ListadoAdministradoresComponent
      },
      {
        path: 'varamientos',
        component: ListadoVaramientoComponent
      }
    ] 
  },
  {
    path: 'buscar-donaciones',
    component: SearchDonacionComponent
  },
  {
    path: 'frame',
    component: IframeComponent
  },
  {
    path: 'calendar',
    component: CalendarioLiberacionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

