// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hasuraKey: 'K1HEop2zkLnHy3MStMLZHRtCjaVveNJr6rceX0uM8CTfcZwQxeQKduFQUF9igNy4',
  graphql: 'https://monitor-graphql-api.hasura.app/v1/graphql',
  monitorApi: 'http://monitorgt.org:8080/monitor-api/monitor/',
  bucket: 'arn:aws:s3:::monitor5a8928f3297344b49d06a5ba55ae11fc231056-dev',
  region: 'us-west-2',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
