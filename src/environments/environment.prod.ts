export const environment = {
  production: true,
  monitorApi: 'http://monitor.org/monitor-api/monitor/',
  bucket: 'arn:aws:s3:::monitor5a8928f3297344b49d06a5ba55ae11fc231056-dev',
  region: 'us-west-2',
};
